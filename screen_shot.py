import glob, os
import time
import subprocess
import pyautogui
global COUNT
COUNT = 0
os.chdir(r"/Users/ankurgupta/devops/python-project/oneassist-lab/logs")

file_dict = dict()


for file in glob.glob("*.log"):
    list_of_results = []
    line_number = 0
    with open(file, "r") as text:
        for line in text:
            line_number += 1
            if "ankur" in line.lower():
                list_of_results.append(line_number)
    file_dict[file] = list_of_results
print (file_dict)
for f, n in file_dict.items():
     
    
    for i in n:
        time.sleep(5)
        proc = subprocess.Popen(["/usr/local/bin/mvim -n {0} +{1} -c 'set cursorline' ".format(f, i)], shell=True) # for mac OS
        time.sleep(3)
        log_screenshot = pyautogui.screenshot()
        f_ext = '.png'
        COUNT = COUNT + 1
        snapshot_name = f + str(COUNT)
        output_folder = "/Users/ankurgupta/devops/python-project/oneassist-lab/screenshot/"
        log_screenshot.save('{} {} {}'.format(output_folder, snapshot_name, f_ext))
        proc_pid = os.system("kill -9 $(ps aux | grep 'set' |grep 'cursor'|awk '{print $2}')")
        time.sleep(15)

