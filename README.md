**Below assignments are performed using jupyter-notebook on macbook with python version 3.8.2**

**#####Assignment1######**

**To count the occurenece of any string in a log files under logs directory.**

- Note: Here the string is "ankur"
- Script - matching_string.py


Thing to do in order to run the script:-
- import glob, os module.
- Need to provide absoulte path of the logs directory as per your working directory(get it by cmd - pwd).

Desire results:-
file name : <count of occurence of a string ("ankur")in a file>


**#######Assignment2########**

**To capture the snapshot of a entry if matches the given string(in this case "ankur") in a file & store it.**

- Note: Here the string is "ankur"
- Script - screen_shot.py


Required editor package

**macos - _mvim_**

**linux - _gvim_**


_**if running on linux - needd to replace mvim with gvim**_

here - proc = subprocess.Popen(["_/usr/local/bin/mvim_ -n {0} +{1} -c 'set cursorline' ".format(f, i)], shell=True) # for mac OS

Thing to do in order to run the script:-
- import glob, os module
- import time
- import subprocess
- import pyautogui
- _Need to provide absoulte path of the logs directory & screenshot directory as per your working directory(get it by cmd - pwd)_.

Desire results:
 filename: <line number of the string found in a file>
 Save the snap in screenshot directory with filename(increment order of number of string occured in a file).png




