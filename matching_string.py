import glob, os
os.chdir("logs")
for file in glob.glob("*.log"):
    with open(file, "r") as text:
        data = text.read().lower()
        count = data.count("ankur")
    print(file, ":", count)